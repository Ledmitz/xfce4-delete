Bash script made for right-click function of XFCE4. Standard delete offers moving to trash only,
but temporarily mounted file systems offer no trash and some can't support it, so I made this
delete script with double confirmation.

xfce4-delete bypasses the trashcan and deleting permanently with a confirmation window first.
- Place in a known system PATH. I added "~/bin" to my paths as is often suggested for user
scripts. "echo $PATH" to see what's already available. -
1) chmod +x delete
2) In Thunar: Edit/"Configure custom actions" and edit or create a new function.
3) Command is "delete %F" (%F = freedesktop.org Files variable). I just titled mine "Delete".
4)"Appearance Conditions" tab: File pattern is "*". Select all types.

Now when you right-click there should be an option to delete. There are 2 confirmations before
removal of files to avoid a terrible mishap.

Ledmitz (2020)
GPL-3.0
